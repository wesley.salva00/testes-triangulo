
from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class Triangle(object):
    def __init__(self,driver):
        self.driver = driver
        self.v1Field = driver.find_element(By.NAME, 'V1')
        self.v2Field = driver.find_element(By.NAME, 'V2')
        self.v3Field = driver.find_element(By.NAME, 'V3')
        self.identifyButton = driver.find_element(By.XPATH, "//input[@type='submit'][@value='Identificar']")
        #self.result = driver.find_element(By.XPATH,"/html/body/div[4]/text()")

        self.triangleType = 0
    
    def set_V1_value(self,value): 
        strValue = str(value)
        return self.v1Field.send_keys(strValue)
    
    def set_V2_value(self,value):
        strValue = str(value)
        return self.v2Field.send_keys(strValue)

    def set_V3_value(self,value): 
        strValue = str(value)
        return self.v3Field.send_keys(strValue)
    
    def click_Identify_Button(self):
        return self.identifyButton.click()


class TriangleIdentifier(unittest.TestCase, Triangle):

    def setUp(self):
        self.driver = webdriver.Chrome('/Users/wesleysalvador/chromedriver/chromedriver')
        self.driver.get('http://www.vanilton.net/triangulo/')
    
    def test_Identify_Isosceles(self):
        v1 = 10
        v2 = 15
        v3 = 10
        trianglePage = Triangle(self.driver)
        trianglePage.set_V1_value(v1)
        trianglePage.set_V2_value(v2)
        trianglePage.set_V3_value(v3)
        trianglePage.click_Identify_Button()
        time.sleep(3)
        triangleType = trianglePage.identify_The_Triangle_Type(v1,v2,v3)
        print (triangleType)

        # self.assertEqual(result, "Isósceles")
        # Não deu pra colocar uma assertiva pq
        # a imagem e o texto que sãos os resultados não possuem identificadores
        # válidos e não consegui fazer um workarround a tempo.

    def test_Identify_Equilateral(self):
        v1 = 10
        v2 = 10
        v3 = 10
        trianglePage = Triangle(self.driver)
        trianglePage.set_V1_value(v1)
        trianglePage.set_V2_value(v2)
        trianglePage.set_V3_value(v3)
        trianglePage.click_Identify_Button()
        time.sleep(3)
        triangleType = trianglePage.identify_The_Triangle_Type(v1,v2,v3)
        print (triangleType)

        # self.assertEqual(result, "Equilátero")
        
    def test_Identify_Scalene(self):
        v1 = 11
        v2 = 15
        v3 = 10
        trianglePage = Triangle(self.driver)
        trianglePage.set_V1_value(v1)
        trianglePage.set_V2_value(v2)
        trianglePage.set_V3_value(v3)
        trianglePage.click_Identify_Button()
        time.sleep(3)
        triangleType = trianglePage.identify_The_Triangle_Type(v1,v2,v3)
        print (triangleType)

        # self.assertEqual(result, "Escaleno")
    
    def test_All_Vertex_Zero(self):
        v1 = 0
        v2 = 0
        v3 = 0
        trianglePage = Triangle(self.driver)
        trianglePage.set_V1_value(v1)
        trianglePage.set_V2_value(v2)
        trianglePage.set_V3_value(v3)
        trianglePage.click_Identify_Button()
        time.sleep(3)
        triangleType = trianglePage.identify_The_Triangle_Type(v1,v2,v3)
        print (triangleType)

        # self.assertNotEqual(result, "Escaleno")
        # self.assertNotEqual(result, "Equilátero")
        # self.assertNotEqual(result, "Isósceles")

    def test_Not_A_Triangle(self):
        v1 = 5
        v2 = 15
        v3 = 20
        trianglePage = Triangle(self.driver)
        trianglePage.set_V1_value(v1)
        trianglePage.set_V2_value(v2)
        trianglePage.set_V3_value(v3)
        trianglePage.click_Identify_Button()
        time.sleep(3)
        triangleType = trianglePage.identify_The_Triangle_Type(v1,v2,v3)
        print (triangleType)

        # self.assertNotEqual(result, "Escaleno")
        # self.assertNotEqual(result, "Equilátero")
        # self.assertNotEqual(result, "Isósceles")
         
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
