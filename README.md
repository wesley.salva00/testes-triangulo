## Ambiente

- Python3 instalado
- Selenium instalado
- Chromedriver instalado https://sites.google.com/chromium.org/driver/
- Ide para execução (preferencialmente)

## Execução 
- Abrir o arquivo main.py
- Executar pela ide clicando em run
- Executar pelo terminal com
python3 main.py

## Observação
- O site triângulo não possui identificadores válidos para a imagem e o texto que são a saída do software. Tanto css_selector quanto x_path não funcionaram, logo não foi possível criar uma assertiva confiável.
